/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include "SURFPrototpyes.h"

void buildIntegralImage(IntegralImage *integralImage,
                         const uint8_t *imageBuffer,
                         const uint16_t imageWidth, const uint16_t imageHeight)
{
    /* loop variables */
    register int i, x, y;
    float rowIntegral;
    
    /* copy propeties */
    integralImage->width  = imageWidth;
    integralImage->height = imageHeight;
    
    /* alloc memory */
    integralImage->integration =
        (float **)malloc(sizeof(float *) * (integralImage->height));
    MALLOC_FAIL_CHECK(integralImage->integration);
    
    for (i = 0; i < integralImage->height; i++)
    {
        integralImage->integration[i] =
        (float *)malloc(sizeof(float) * (integralImage->width));
        MALLOC_FAIL_CHECK(integralImage->integration[i]);
    }
    
    /* evaluate the integration of the first row */
    rowIntegral = 0.0f;
    for (x = 0; x < imageWidth; x++)
    {
        rowIntegral += imageBuffer[x] / 255.0f;
        
        integralImage->integration[0][x] = rowIntegral;
    }
    
    /* evaluate the integration values */
    for (y = 1; y < imageHeight; y++)
    {
        rowIntegral = 0.0f;
        for (x = 0; x < imageWidth; x++)
        {
            rowIntegral += imageBuffer[imageWidth * y + x] / 255.0f;
            
            /* integral image is rowsum + value above */
            integralImage->integration[y][x] =
            rowIntegral + integralImage->integration[y - 1][x];
        }
    }
}

void releaseintegralImage(IntegralImage *integralImage)
{
    /* clean up */
    register int i;
    
    for (i = 0; i < integralImage->height; i++)
    {
        free(integralImage->integration[i]);
        integralImage->integration[i] = NULL;
    }
    
    free(integralImage->integration);
    integralImage->integration = NULL;
}

float integral(const IntegralImage *integralImage, const int row,
               const int col, const int rows, const int cols)
{
    int r1, c1, r2, c2;
    float A, B, C, D;
    
    r1 = __MIN_(row, integralImage->height) - 1;
    c1 = __MIN_(col, integralImage->width) - 1;
    r2 = __MIN_(row + rows, integralImage->height) - 1;
    c2 = __MIN_(col + cols, integralImage->width) - 1;
    
    A = ((r1 >= 0) && (c1 >= 0)) ? (integralImage->integration[r1][c1]):(0.0f);
    B = ((r1 >= 0) && (c2 >= 0)) ? (integralImage->integration[r1][c2]):(0.0f);
    C = ((r2 >= 0) && (c1 >= 0)) ? (integralImage->integration[r2][c1]):(0.0f);
    D = ((r2 >= 0) && (c2 >= 0)) ? (integralImage->integration[r2][c2]):(0.0f);
    
    return __MAX_(0.0f, A - B - C + D);
}

