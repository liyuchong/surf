/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>

#include "ImageIO.h"
#include "Memory.h"

struct error_mgr
{
    struct jpeg_error_mgr pub;
    jmp_buf               setjmp_buffer;
};

typedef struct error_mgr *error_ptr;

void error_exit(j_common_ptr cinfo)
{
    error_ptr err = (error_ptr)cinfo->err;
    
    (*cinfo->err->output_message)(cinfo);
    
    longjmp(err->setjmp_buffer, 1);
}

Image *decodeFromFileJPEG(const char *fileName)
{
    Image *image = (Image *)malloc(sizeof(Image));
    MALLOC_FAIL_CHECK(image);
    
    struct jpeg_decompress_struct cinfo;
    struct error_mgr jerr;
    
    FILE *infile;
    
    if ((infile = fopen(fileName, "rb")) == NULL)
    {
        fprintf(stderr, "can't open %s\n", fileName);
        return NULL;
    }
    
    cinfo.err = jpeg_std_error(&jerr.pub);
    jerr.pub.error_exit = error_exit;
    
    if (setjmp(jerr.setjmp_buffer))
    {
        jpeg_destroy_decompress(&cinfo);
        fclose(infile);
        return NULL;
    }
    
    jpeg_create_decompress(&cinfo);
    jpeg_stdio_src(&cinfo, infile);
    
    (void)jpeg_read_header(&cinfo, TRUE);
    (void)jpeg_start_decompress(&cinfo);
        
    /* alloc image data space */
    image->pixel =
    (uint8_t *)malloc(cinfo.output_width * cinfo.output_height * 4 *
                      sizeof(uint8_t));
    
    MALLOC_FAIL_CHECK(image->pixel);

    uint8_t *buff = (uint8_t *)
        malloc(cinfo.output_width * cinfo.output_components);
    
    MALLOC_FAIL_CHECK(buff);
    
    int           row        = 0;
    int           infoBase   = 0;
    int           bufferBase = 0;
    while (cinfo.output_scanline < cinfo.output_height)
    {
        jpeg_read_scanlines(&cinfo, &buff, 1);
        
        for (int i = 0; i < cinfo.output_width; i++)
        {
            infoBase   = row * cinfo.output_width * 4 + i * 4;
            bufferBase = i * cinfo.output_components;
            
            uint8_t r = buff[bufferBase];
            uint8_t g = buff[bufferBase + 1];
            uint8_t b = buff[bufferBase + 2];
            
            image->pixel[infoBase]     = r;
            image->pixel[infoBase + 1] = g;
            image->pixel[infoBase + 2] = b;
            image->pixel[infoBase + 3] = 255;
        }
        
        row++;
    }
    
    image->imageWidth  = cinfo.output_width;
    image->imageHeight = cinfo.output_height;
    
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    
    fclose(infile);
    free(buff);
    
    return image;
}
